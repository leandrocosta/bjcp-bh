[//]: <> (Apresentação sobre Drinkability para o Grupo de Estudos BJCP de BH em 10/07/2018)
[//]: <> (https://raw.githubusercontent.com/gitpitch/feature-demo/customize-image-size/PITCHME.md)
## *<span class="highlight">Drinkability</span>*
---
<!-- .slide: class="flex r-left-aligned" -->
@div[left-50]
<img src="assets/BJCP_2008_pt-BR.jpg" alt="BJCP 2008 pt-BR" width="80%">
https://www.bjcp.org/intl/2008styles-PT.pdf
@divend

@div[right-50]
@size[.65em](*<span class="highlight">Drinkability</span>*: Facilidade em beber.)
@ul
- @size[.65em](Termo utilizado para designar a palatabilidade geral de uma cerveja.)
- @size[.65em](Quanto maior o “*<span class="highlight">drinkability</span>*” mais vontade você tem de beber e continuar bebendo determinada cerveja.)
- @size[.65em](Cervejas de baixa *<span class="highlight">drinkability</span>* não devem ser consideradas “piores” do que cervejas de alta *<span class="highlight">drinkability</span>*.)
@ulend
@divend
---
<!-- .slide: class="flex" -->
@div[left-40]
<img src="assets/BJCP_2015.jpg" alt="BJCP 2015" width="80%">
https://www.bjcp.org/docs/2015_Guidelines_Beer.pdf
@divend

@div[right-60]
21B. Specialty IPA
<br><br>
<pre>
Strength classifications:
Session – ABV: 3.0 – 5.0%
Standard – ABV: 5.0 – 7.5%
Double – ABV: 7.5 – 10.0%
</pre>
<br>
Deve ter uma boa <span class="highlight">*drinkability*</span>, independente do formato.
<!--Should have good <span class="highlight">drinkability</span>, regardless of the form.-->
@divend
---
<!-- .slide: class="flex" -->
@div[left-40]
<img src="assets/BJCP_2015.jpg" alt="BJCP 2015" width="80%">
https://www.bjcp.org/docs/2015_Guidelines_Beer.pdf
@divend

@div[right-60]
22A. Double IPA
<br><br>
<span class="highlight">*Drinkability*</span> é uma característica importante; não deve ser uma cerveja pesada (*sipping beer*).
<!--<span class="highlight">Drinkability</span> is an important characteristic; this should not be a heavy, sipping beer.-->
@divend
---
<!-- .slide: class="flex" -->
@div[left-40]
<img src="assets/BJCP_2015.jpg" alt="BJCP 2015" width="80%">
https://www.bjcp.org/docs/2015_Guidelines_Beer.pdf
@divend

@div[right-60]
22C. American Barleywine
<br><br>
[...] geralmente possui mais dulçor residual que uma Double IPA, o que afeta a <span class="highlight">*drinkability*</span> [...].
<!--[...] typically has more residual sweetness than a Double IPA, which affects the overall <span class="highlight">drinkability</span> [...].-->
@divend
---
<!-- .slide: class="flex" -->
@div[left-30]
![BA 2018](assets/BA_feature1.jpg)
@divend

@div[right-70]
BA 2018 - Belgian-Style Quadrupel
<br><br>
São bem equilibradas com uma <span class="highlight">*drinkability*</span> apropriada para se degustar (*savoring/sipping-type*).
<!--They are well balanced with savoring/sipping-type <span class="highlight">drinkability</span>.-->
https://www.brewersassociation.org/resources/brewers-association-beer-style-guidelines/
@divend
---
<!-- .slide: class="flex" -->
@div[left-50]
<img src="assets/TastingBeer.jpg" alt="Tasting Beer" width="80%">
http://randymosher.com/Tasting-Beer
@divend

@div[right-50]
Nas palavras de August Busch III, “Você para de beber quando sabe que está na hora, mas você não quer parar: isso é <span class="highlight">*drinkability*</span>”.
<!--In the words of August Busch III, “You stop drinking because you know it's time to stop but you don't want to: that's <span class="highlight">drinkability</span>”.-->
@divend
---
<!-- .slide: class="flex" -->
@div[left-50]
<img src="assets/TastingBeer.jpg" alt="Tasting Beer" width="80%">
http://randymosher.com/Tasting-Beer
@divend

@div[right-50]
Existem *Saisons* de várias intensidades; algumas das mais fortes levam açúcar pra melhorar a <span class="highlight">*drinkability*</span>.
<!--Many saison brewers offer a range of strengths; some of the stronger ones are brewed with sugar to improve <span class="highlight">drinkability</span>.-->
@divend
---
<!-- .slide: class="flex" -->
@div[left-40]
<img src="assets/BrewingClassicStyles.jpg" alt="Brewing Classic Styles" width="75%">
<br>
[Brewing Classic Styles](https://www.brewerspublications.com/products/brewing-classic-styles-80-winning-recipes-anyone-can-brew)
@divend

@div[right-60]
@size[.7em](Keys to Brewing American Pale Ale)
<br><br>
O nível de carbonatação e a quantidade de álcool também afetam a <span class="highlight">*drinkability*</span>.
<br>
Uma Pale Ale muito intensa ou muito carbonatada é, geralmente, uma cerveja de menor <span class="highlight">*drinkability*</span>.
<!--The level of carbonation and the amount of alcohol also affect <span class="highlight">drinkability</span>.
A really big or highly carbonated pale ale is often a less drinkable pale ale.-->
@divend
---
<!-- .slide: class="flex" -->
@div[left-30]
<img src="assets/BeerTapIntoTheArtAndScienceOfBrewing.jpg" alt="Beer - Tap Into the Art and Science of Brewing" width="80%">
@divend

@div[right-70]
<span class="highlight">Drinkability</span> possui significados diferentes pra pessoas diferentes. Pra mim é um caso de “esta cerveja estava boa, eu poderia beber outra dessa”.
<!--<span class="highlight">Drinkability</span> means different things to different people. For me it is a case of “that was a good pint; you know, I could really drink another of those”.-->
<br>
[Beer - Tap Into the Art of Science of Brewing](https://global.oup.com/academic/product/beer-9780195305425)
@divend
---
<!-- .slide: class="flex" -->
@div[left-30]
<img src="assets/BeerTapIntoTheArtAndScienceOfBrewing.jpg" alt="Beer - Tap Into the Art and Science of Brewing" width="80%">
@divend

@div[right-70]
@size[.85em](Pra outros é uma questão de saciedade. “Eu não beberia outra dessa”, seja por que estão cheios - de líquido ou dióxido de carbono - ou é um caso de “é uma cerveja tão complexa que saturou meus sentidos” [...].)
<!--@size[.85em](For others it all has to do with satiety. “I really couldn’t take in another of those”, whether it is because they are filled - with fluid or carbon dioxide - or whether it’s a case of “mmm, that is so fully flavored that my senses are saturated” [...].)-->
<br>
[Beer - Tap Into the Art of Science of Brewing](https://global.oup.com/academic/product/beer-9780195305425)
@divend
---
<!-- .slide: class="flex" -->
@div[left-30]
<img src="assets/BeerTapIntoTheArtAndScienceOfBrewing.jpg" alt="Beer - Tap Into the Art and Science of Brewing" width="80%">
@divend

@div[right-70]
<span class="highlight">*Drinkability*</span>
<br><br>
A propriedade da cerveja que faz o cliente decidir se vale a pena comprá-la novamente ou não.
<!--The property of beer that determines whether or not a customer judges it worthy of repurchase.-->
<br>
[Beer - Tap Into the Art of Science of Brewing](https://global.oup.com/academic/product/beer-9780195305425)
@divend
---
<!-- .slide: class="flex" -->
@div[left-40]
<img src="assets/14c_pliny.jpg" alt="14C. Imperial IPA - Russian River Pliny the Elder" width="80%">
https://www.bjcp.org/course/ClassicStyles.php
@divend

@div[right-60]
14C. Imperial IPA
<br>
@size[.8em](Russian River Pliny the Elder)
<br><br>
“A <span class="highlight">*drinkability*</span> é ótima.”
<!--“<span class="highlight">Drinkability</span> is great.”-->
<br><br>
@size[.6em](Gordon Strong, 2009)
@divend
---
<!-- .slide: class="flex" -->
@div[left-40]
<img src="assets/14c_port_brewing_hop15.jpg" alt="14C. Imperial IPA - Port Brewing Hop 15" width="80%">
https://www.bjcp.org/course/ClassicStyles.php
@divend

@div[right-60]
14C. Imperial IPA
<br>
Port Brewing Hop 15
<br><br>
“Corpo, final e álcool estão um pouco altos -- menos melhoraria a <span class="highlight">*drinkability*</span>.”
<!--“Body, finish and alcohol are a bit high -- less would improve <span class="highlight">drinkability</span>.”-->
<br><br>
@size[.6em](Gordon Strong, 2009)
@divend
---
<!-- .slide: class="flex" -->
@div[left-40]
<!-- https://www.bjcp.org/course/sheets/19c_bigfoot_2008.pdf -->
<img src="assets/19c_bigfoot_2008.jpg" alt="19C. American Barleywine - Sierra Nevada Bigfoot 2008" width="80%">
https://www.bjcp.org/course/ClassicStyles.php
@divend

@div[right-60]
@size[.8em](19C. American Barleywine)
<br>
@size[.8em](Sierra Nevada Bigfoot 2008)
<br><br>
“Bem atenuada, uma certa secura que favorece a <span class="highlight">*drinkability*</span>.”
<!--“Well attenuated, dryish finish aids <span class="highlight">drinkability</span>.”-->
<br><br>
@size[.6em](Gordon Strong, 2008)
@divend
---
<!-- .slide: class="flex" -->
@div[left-40]
<!-- https://www.bjcp.org/course/sheets/13e_old_no_38.pdf -->
<img src="assets/13e_old_no_38.jpg" alt="13E. American Stout - North Coast Old #38 Stout" width="80%">
https://www.bjcp.org/course/ClassicStyles.php
@divend

@div[right-60]
13E. American Stout
<br>
@size[.8em](North Coast Old #38 Stout)
<br><br>
“O lúpulo está um pouco restrito, mas isso ajuda na <span class="highlight">*drinkability*</span>.”
<!--“Hops are a little restrained but that improves <span class="highlight">drinkability</span>.”-->
<br><br>
@size[.6em](Gordon Strong, 2009)
@divend
---
<!-- .slide: class="flex" -->
@div[left-40]
<img src="assets/EricCousineauScoresheet_BalticPorter.jpg" alt="12C. Baltic Porter (Eric Cousineau)" width="80%">
https://ericbrews.com/2016/04/11/becoming-a-beer-judge-bjcp/
@divend

@div[right-60]
12C. Baltic Porter
<br><br>
@size[.9em](“Eu recomendaria ajustar o nível de amargor para reduzi-lo e aumentar a <span class="highlight">*drinkability*</span> [...].”)
<!--“I would suggest tweaking the hop bitterness level to reduce overall bitterness and increase the <span class="highlight">drinkability</span> of the beer.”-->
<br><br>
@size[.6em](Eric Cousineau, 2014)
@divend
---
<!-- .slide: class="flex" -->
@div[left-40]
![Brasil Beer Guide](assets/BrasilBeerGuide2.jpg)
[Brasil Beer Guide](https://play.google.com/store/apps/details?id=br.com.mobilus.brasilbeerguide)
@divend

@div[right-60]
Dum Petroleum
<br><br>
@size[.9em](“[...] <span class="highlight">drinkability</span> médio-alta, apesar do teor alcoólico de 12%.”)
<br><br>
@size[.6em](Brasil Beer Guide, 2017)
@divend

Note:
Dum Petroleum Amburana
<br><br>
“<span class="highlight">Drinkability</span> perigosamente alta.”
<br>
Dum Petroleum Castanheira
<br><br>
“A <span class="highlight">drinkability</span> é médio-alta, apesar do teor alcoólico de 12%.”
---
<!-- .slide: class="flex" -->
@div[left-40]
![Brasil Beer Guide](assets/BrasilBeerGuide2.jpg)
[Brasil Beer Guide](https://play.google.com/store/apps/details?id=br.com.mobilus.brasilbeerguide)
@divend

@div[right-60]
Wals Niobium
<br><br>
“Média-alta <span class="highlight">drinkability</span>.”
<br><br>
@size[.6em](Brasil Beer Guide, 2016)
@divend
---
<!-- .slide: class="flex" -->
@div[left-40]
![Brasil Beer Guide](assets/BrasilBeerGuide2.jpg)
[Brasil Beer Guide](https://play.google.com/store/apps/details?id=br.com.mobilus.brasilbeerguide)
@divend

@div[right-60]
Loba Russian Imperial Stout
<br><br>
“A <span class="highlight">drinkability</span> é média.”
<br><br>
@size[.6em](Brasil Beer Guide, 2016)
@divend
---